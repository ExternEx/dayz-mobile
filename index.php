<?
include($_SERVER['DOCUMENT_ROOT'].'/core/core.php'); //Подключаем ядро скрипта

if(empty($_GET['controller']))
{
    $controller = 'index';
}
else
{
    $controller = htmlspecialchars(trim($_GET['controller']));
}
route($controller); //Загружаем шаблон и контроллер
