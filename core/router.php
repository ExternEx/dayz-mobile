<?
/*
* @name Функция подключения шаблона и контроллера
* @author ExternEx
*/
function route($controller_name)
{
    global $db;
    global $user;
    $path_to_controllers = $_SERVER['DOCUMENT_ROOT'].'/controllers/'; //Путь к контроллерам
     if($controller_name == null)
    {
        echo 'Controller name is empty!<br/>';
    }
    elseif(is_file($path_to_controllers.$controller_name.'.php') == false)
    {
        echo 'Failed load the controller "'.$controller_name.'"!<br/>';
    }
    else
    {
        include($path_to_controllers.$controller_name.'.php'); //Загружаем контроллер
    }
    include($_SERVER['DOCUMENT_ROOT'].'/core/head.php');
    $path_to_views = $_SERVER['DOCUMENT_ROOT'].'/views/'; //Путь к шаблонам
     if($controller_name == null)
    {
        echo 'View name is empty!<br/>';
    }
    elseif(is_file($path_to_views.$controller_name.'.tpl') == false)
    {
        echo 'Failed load the view "'.$controller_name.'"!<br/>';
    }
    else
    {
        include($path_to_views.$controller_name.'.tpl'); //Загружаем шаблон
    }
    include($_SERVER['DOCUMENT_ROOT'].'/core/foot.php');
    return true;
}

