<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/router.php'); //Подключаем роутер
$config = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/core/main.cfg'); //Загружаем конфигурационный файл
$dsn = "mysql:host=".$config['DBHost'].";dbname=".$config['DBName'].";charset=".$config['DBCharset'];
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
$db = new PDO($dsn, $config['DBUser'], $config['DBUserPassword'], $options);//Подключаемся к MySQL через PDO
session_start();
ob_start();
if(!empty($_COOKIE['login']) && !empty($_COOKIE['password'])) 
{

	$login = filter($_COOKIE['login']);
	$password = filter($_COOKIE['password']);
	
	$user = $db->prepare('SELECT * FROM `users` WHERE `password` = ? AND `login` = ? LIMIT 1');
	$user->execute(array($password, $login));
	
	unset($login, $password);

	if(!$user = $user->fetch()){
		unset($_COOKIE['login']);
		unset($_COOKIE['password']);
		header("Location: /");
		exit;
	} 
}

function filter($var)
{
    if(is_string($var))
    {
        $filtervar = htmlspecialchars(trim($var));
    }
    else
    {
        $filtervar = intval($var);
    }
    return $filtervar;
}

function message($message)
{
    return $_SESSION['message'] = $message;
    
}

function redirect($url)
{
    header("Location: ".$url);
    exit;
    return true;
}

function mode($mode)
{
    global $user;
    if($mode == 'user')
    {
        if(!user)
        {
            message("Доступ закрыт для гостей!");
            redirect("/");
        }
    }
    else
    {
        if($user)
        {
            message("Доступ закрыт для пользователей!");
            redirect("/my");
        }
    }
}

function page($k_page=1)
{
    $page=1;
    if (isset($_GET['str']))
    {
        if ($_GET['str']=='end') $page=intval($k_page);
        elseif(is_numeric($_GET['str'])) $page=intval($_GET['str']);
    }
    if ($page<1) $page=1;
    if ($page>$k_page) $page=$k_page;
    return $page;
}

function k_page($k_post=0,$k_p_str=20)
{
    if ($k_post!=0)
    {
        $v_pages=ceil($k_post/$k_p_str);
        return $v_pages;
    }
    else return 1;
}

function str($link='?',$k_page=1,$page=1){
    ?><div class="page"><font size="3" color="white"><?
    if ($page<1) $page=1;
    if ($page!=1) ?> <a href="<?=$link;?>str=1">«</a> <?
    if ($page!=1)
    {
?> <a href="<?=$link;?>str=1">1</a> <?
    }
    else
    { 
        ?> <span class="spage">1</span> <?
    }
    for ($ot=-3; $ot<=3; $ot++)
    {
        if ($page+$ot>1 && $page+$ot<$k_page)
        {
            if ($ot!=0)
            {
                ?> <a href="<?=$link;?>str=<?=($page+$ot);?>"><?=($page+$ot);?></a> <?
            }
            else
            {
            ?><span class="spage"><?=($page+$ot);?></span><?
            }
        }
    }
    if ($page!=$k_page)
    {
        ?> <a href="<?=$link;?>str=end"><?=$k_page;?></a> <?
    }
    elseif ($k_page>1)
    {
        ?> <span class="spage"><?=$k_page;?></span> <?
    }
    if ($page!=$k_page)
    {
        ?><a href="<?=$link;?>str=end">»</a><?
    }
    ?></font></div><?
}

function user($id,$column)
{
    global $db;
    $id = filter($id);
    $us = $db->prepare("SELECT * FROM `users` WHERE `id` = ?");
    $us->execute(array($id));
    $us = $us->fetch();
    return $us[$column];
}
