<?
$title = "Сохранение";
mode('user');
if($user['save'] == 1)
{
    redirect("/my");
}
if(isset($_GET['save']))
{
    $login = filter($_POST['login']);
    $password = filter($_POST['password']);
    $email = filter($_POST['email']);
    $sex = filter($_POST['sex']);
    $lg_is = $db->prepare("SELECT * FROM `users` WHERE `login` = ?");
    $lg_is->execute(array($login));
    $el_is = $db->prepare("SELECT * FROM `users` WHERE `email` = ?");
    $el_is->execute(array($email));
    if($lg_is->rowCount() > 0) $err = 'Игрок с таким именем уже есть!';
    elseif($el_is->rowCount() > 0) $err = 'Игрок с таким электронным адресом уже есть!';
    elseif(mb_strlen($login) < 3 OR mb_strlen($login) > 64) $err = 'Длина имени должна быть в пределах 3-64 символов!';
    elseif(mb_strlen($password) < 3 OR mb_strlen($password) > 64) $err = 'Длина пароля должна быть в пределах 3-64 символов!';
    elseif(mb_strlen($email) < 3 OR mb_strlen($email) > 255) $err = 'Длина электронного адреса должна быть в пределах 3-64 символов!';
    if(!$err)
    {
        $sql = $db->prepare("UPDATE `users` SET `login` = ?, `password` = ?, `email` = ?, `sex` = ?, `save` = ?, `baks` = ? WHERE `id` = ?");
        $sql->execute(array($login,md5($password),$email,$sex,1,50,$user['id']));
        setcookie("login",$login,time()+86400*365);
        setcookie("password",md5($password),time()+86400*365);
        message("Успешное сохранение!");
        redirect("/my");
    }
    else
    {
        message($err);
        redirect("/save");
    }
}