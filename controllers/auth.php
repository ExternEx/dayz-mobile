<?
$title = 'Авторизация';
mode('guest');
if(isset($_GET['auth']))
{
    $login = filter($_POST['login']);
    $password = filter($_POST['password']);
    $is_user = $db->prepare("SELECT `id`,`password` FROM `users` WHERE `login` = ?");
    $is_user->execute(array($login));
    $true_pass = $is_user->fetch();
    if($is_user->rowCount() != 1) $err = 'Пользователя не существует!';
    elseif(md5($password) != $true_pass['password']) $err = 'Неверный пароль!';
    if(!$err)
    {
        setcookie("login",$login,time()+86400);
        setcookie("password",md5($password),time()+86400);
        $_SESSION['message'] = 'Авторизация успешна!';
        header("Location: /");
        exit;
    }else{
        $_SESSION['message'] = $err;
        header("Location: /auth");
        exit;
    }
}